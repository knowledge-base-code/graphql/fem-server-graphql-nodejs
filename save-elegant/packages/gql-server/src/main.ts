import { ApolloServer } from "apollo-server";
import gql from "graphql-tag";

const typeDefs = gql`
    type User {
        email: String!
        avatar: String
        friends: [User]!
    }

    type Query {
        me: User!
    }
`

const resolvers = {
    Query: {
        me: () => ({
            email: 'yoda@master.org',
            avatar: 'cat.jpg',
            friends: []
        })
    }
}

new ApolloServer({
    typeDefs, resolvers
})
    .listen(4000)
    .then(({url}) => console.info({ listening: url}))
    .catch(err => console.error({ err }))